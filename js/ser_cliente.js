$(document).ready(function () {
	$('#ser_cliente').validate({
		ignore: {}
		, rules: {
			nombre: {
				required: true
			}
			, email: {
				required: true
				, validEmail: true
			, }
			, empresa: {
				required: true
			}
			, rut: {
				required: true
			, }
			, telefono: {
				required: true
				, maxlength: 11
				, minlength: 9
			}
			, "hiddenRecaptcha": {
				required: function () {
					if (grecaptcha.getResponse() == '') {
						return true;
					}
					else {
						return false;
					}
				}
			}
		, }
		, messages: {
			rut: {
				required: "Debe ingresar un RUT."
			, }
		}
	});
	// programmatically check the entire form using the `.valid()` method.
	$('#enviar').on('click', function () {
		$('#ser_cliente').valid();
	});
	$('#rut').Rut({
		on_error: function () {
			$('#rut').addClass("error");
			$("#rut").after('<label id="rut-error" class="error" for="rut">Debe ingresar un RUT válido.</label>');
		}, //				on_success: function(){
		//					$('#rut').removeClass("error")
		//				},
		format_on: 'keyup'
	});
});