$(document).ready(function () {
    $('#beneficiarios').validate({
        ignore: {}
        , rules: {
            rut: {
                required: true
            }
            , nombre: {
                required: true
            , }
            , apellido: {
                required: true
            }
            , monto: {
                required: true
            , }
        , }
        , messages: {
            rut: {
                required: "Debe ingresar un RUT."
            , }
        }
    });
    $('#siguiente').on('click', function () {
        $('#beneficiarios').valid();
    });
    $('#rut').Rut({
        on_error: function () {
            $('#rut').addClass("error");
            $("#rut").after('<label id="rut-error" class="error" for="rut">Debe ingresar un RUT válido.</label>');
        }, //				on_success: function(){
        //					$('#rut').removeClass("error")
        //				},
        format_on: 'keyup'
    });
});

$(document).ready(function () {
    var n_filas = $('#fila').children().size();
    var n_filas = n_filas + 2;
    $("#agregar-beneficiario").click(function () {
        var n = n_filas++;
        $("#tabla-beneficiarios").append("<div class='fila beneficiario'><div class='col col-rut'><label class='visible-xs visible-md'>Rut</label><input id='rut_" + (n) + "' name='rut_" + (n) + "' placeholder='11.111.111-1' type='text'></div><div class='col col-nombre'><label class='visible-xs visible-md'>Nombre</label><input id='nombre_" + (n) + "' name='nombre_" + (n) + "' placeholder='Ingresar Nombre' type='text'></div><div class='col col-apellido'><label class='visible-xs visible-md'>Apellido</label><input id='apellido_" + (n) + "' name='apellido_" + (n) + "' placeholder='Ingresar Apellido' type='text'></div><div class='col col-nueva-tarjeta col-lg-alignment-center'><label class='visible-xs visible-md'>Tarjeta Nueva</label><input name='nuevatarjetaIds[]' type='checkbox' id='tarjeta-nueva' checked value='1'></div><div class='col col-monto'><label class='visible-xs visible-md'>Monto</label><input id='monto_" + (n) + "' name='monto_" + (n) + "' placeholder='$0' type='number'></div><div class='col col-saldo'><label class='visible-xs visible-md'>Saldo</label><span>$3.600</span></div><div class='col col-eliminar-beneficiario'><a class='eliminar-beneficiario' href='#tabla-beneficiarios'>Eliminar</a></div></div>");
        $('input[type="radio"], input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
            , radioClass: 'iradio_flat-blue'
        });
        $('input[name*="rut_' + (n) + '"]').rules('add', {
            required: true
            , messages: {
                required: "Debe ingresar un RUT."
            }
        });
        $('input[name*="nombre_' + (n) + '"]').rules('add', {
            required: true
        });
        $('input[name*="apellido_' + (n) + '"]').rules('add', {
            required: true
        });
        $('input[name*="monto_' + (n) + '"]').rules('add', {
            required: true
        });
        $('#rut_' + (n) + '').Rut({
            on_error: function () {
                $('#rut_' + (n) + '').addClass("error");
                $('#rut_' + (n) + '').after("<label id='rut-error' class='error' for='rut'>Debe ingresar un RUT válido.</label>");
            }, //				on_success: function(){
            //					$('#rut').removeClass("error")
            //				},
            format_on: 'keyup'
        });
		
		
    });
});


vex.defaultOptions.className = 'vex-theme-plain';
vex.dialog.buttons.YES.text = 'Sí';
vex.dialog.buttons.NO.text = 'No';


$("#tabla-beneficiarios").on("click", ".eliminar-beneficiario", function () {
	var elimina = $(this);
	
    vex.dialog.confirm({
        message: '¿Desea eliminar este beneficiario?'
        , callback: function (value) {
			if (value) {
				elimina.closest('.beneficiario').remove();	
			}
			
        }
    })
});