$(document).ready(function () {
    $('#reestablecer').validate({
        rules: {contrasena: {
                required: true
            }, repite_contrasena: {
                required: true,
            equalTo: 'contrasena'}
        , }
    });
    // programmatically check the entire form using the `.valid()` method.
    $('#modificar').on('click', function () {
        $('#reestablecer').valid();
    });
});