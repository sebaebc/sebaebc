/**
 * Ventana Modal
 */

var modalInit = function(element) {
    if (typeof element == 'object') {
        var data = element.modalTarget ? element : $(element).data();
    }

    var body            = $("body"),
        resize          = false,
        target          = $('[data-modal-source="'+data.modalTarget+'"]'),
        targetContent   = $(".modal-buscador-detalles",target),
        template        =  '<div class="wmModal">'+
                                '<div class="modal-content-wrapper">'+
                                    '<div class="close-modal"></div>'+
                                    '<div class="modal-content"></div>'+
                                '</div>'+
                            '</div>',
        templateImage   = '<image src="" title="" />';

    /**
     * RESIZE EVENT
     *
     * @description Listen for window resize event, to resize the modal window according
     *              to the new window dimensions
     */
    var resizer = function(obj){
        var mobileLimit = 1024,
            wCW         = newModal.outerWidth() - 50,
            hCW         = newModal.outerHeight() - 50,
            fit         = "";

        if (image) {
            var wContent    = image[0].naturalWidth,
                hContent    = image[0].naturalHeight,
                fit         = getObjectFitSize(true,wCW, hCW, wContent, hContent);
        } else if (obj){
            var wContent    = obj.outerWidth(),
                hContent    = obj.outerHeight(),
                fit         = { width: wContent, height: hContent };
        }


        // Mobile Width & Height
        var winW            = $(window).width(),
            winH            = $(window).height(),
            mobileStyle     = {};

        if (winW < fit.width) {
            fit = {
                width: winW - 50,
                height: fit.height
            }
            mobileStyle["width"] = "100%";
        } else {
            // obj.attr("style","");
        }
        if (winH < fit.height) {
            fit = {
                width: fit.width,
                height: winH - 50
            }
            mobileStyle["height"] = "100%";
            mobileStyle["overflow-y"] = "scroll";
        } else {
            obj.attr("style","");
        }

        var w       = fit.width,
            h       = fit.height,
            style   = {"width":w+"px", "height":h+"px"};

        contentWrapper.attr('style','').css(style);
        obj.css(mobileStyle);
    }

    // New Element
    if (target) {
        var newModal    = target,
            resize      = targetContent;    
    } else {
        var newModal    = $(template);
    }
    var contentWrapper  = $(".modal-content-wrapper",newModal),
        content         = $(".modal-content",contentWrapper);    


    if (data.onTheFly == true){
        if (data.sourceType == "image"){
            var image = $(templateImage);
            image.attr("src",data.source);
        }
        $(".modal-content",newModal).append(image);

        // Add New Modal to DOM only if "on-the-fly"
        body.prepend(newModal);
    }

    // Resize Modal
    // if (targetContent.length) {
    //     resizer(targetContent);
    // }


    // Open New Modal Window
    //---------------------------------------------------
    var style = {
        "display":"flex"
    };
    newModal.css(style);

    var open = setTimeout(function(){
        newModal.addClass("visible").one("transitionend",function(){
            resizer(resize);
        });
    },10);


    // Bind Close Click Event
    //---------------------------------------------------
    newModal.on("click",".close-modal",function(){
        $(window).off("resize");
        newModal.off("click",".close-modal");
        resizer(resize);

        var close = setTimeout(function(){
            newModal.removeClass("visible").one("transitionend", function(){
                var style = {
                    "display":"none"
                };
                newModal.css(style);

                $(".fullsize-preview",newModal).remove();
                contentWrapper.removeClass("fullsizepreview");
            });
        },10);
    });

    $(window).on("resize",function(){
        resizer(resize);
    });
}

$(document).ready(function(){
    $('[data-toggle="modal"]').on("click",function(e){
        e.preventDefault();
        modalInit(this);
    });

    $(".local-gallery").on("click",".local-gallery-image-thumbnail",function(){
        var el          = $(this),
            parent      = el.parents(".wmModal"),
            image       = el.data("image"),
            pos         = el.position(),
            template    =   '<div class="fullsize-preview">'+
                            '<div class="fullsize-preview-container">'+
                            '<div class="close-preview"><i class="icon-times"></i></div>'+
                            '<img src="'+image+'" />'+
                            '</div>'+
                            '</div>',
            style       = {
                "top":pos.top+"px",
                "left":pos.left+"px"
            };

        $(".modal-content-wrapper",parent).prepend(template);
        var preview = $(".fullsize-preview", parent);
        preview.css(style);

        var t = setTimeout(function(){
            preview.addClass("active");
            $(".modal-content-wrapper",parent).addClass("fullsizepreview");
        }, 100);


        $(".close-preview",preview).on("click",function(){
            var t = setTimeout(function(){
                preview.removeClass("active");
                $(".modal-content-wrapper",parent).removeClass("fullsizepreview").one("transitionend",function(){
                    preview.remove();
                });
            }, 10);
        });


    });
});

function runModalFromMap(element){
    modalInit(element);
}

function getObjectFitSize(contains /* true = contain, false = cover */, containerWidth, containerHeight, width, height){
    var doRatio = width / height;
    var cRatio = containerWidth / containerHeight;
    var targetWidth = 0;
    var targetHeight = 0;
    var test = contains ? (doRatio > cRatio) : (doRatio < cRatio);

    if (test) {
        targetWidth = containerWidth;
        targetHeight = targetWidth / doRatio;
    } else {
        targetHeight = containerHeight;
        targetWidth = targetHeight * doRatio;
    }

    return {
        width: targetWidth,
        height: targetHeight,
        x: (containerWidth - targetWidth) / 2,
        y: (containerHeight - targetHeight) / 2
    };
}