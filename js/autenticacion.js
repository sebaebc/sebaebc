$(document).ready(function () {
    $('#ingreso').validate({
        rules: {
            usuario: {
                required: true
            }
            , contrasena: {
                required: true
            }
            , rut: {
                required: true
            , }
        , }
        , submitHandler: function (form) {
            window.location = "./detalle_compra.html";
        }
    });
    // programmatically check the entire form using the `.valid()` method.
    $('#enviar').on('click', function () {
        $('#registro').valid();
    });
    $('#contrasena_olvidada').validate({
        rules: {
            rut_olvidado: {
                required: true
            , }
        , }
        , submitHandler: function (form) {
            window.location = "./reestablecer.html";
        }
    });
    // programmatically check the entire form using the `.valid()` method.
    $('#reestablecer').on('click', function () {
        $('#contrasena_olvidada').valid();
    });
    $('#registro').validate({
        ignore: []
        , rules: {
            nombre: {
                required: true
            }
            , apellido: {
                required: true
            }
            , rut_registrarme: {
                required: true
                , rut: true
            , }
            , email: {
                required: true
                , validEmail: true
            , }
            , repite_email: {
                validEmail: true
                , equalTo: "#email"
                , required: true
            , }
            , contrasena_registro: {
                required: true
            , }
            , repite_contrasena_registro: {
                required: true
                , equalTo: 'contrasena_registro'
            , }
            , "hiddenRecaptcha": {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        , }
        , submitHandler: function (form) {
            window.location = "./detalle_compra.html";
        }
    });
    // programmatically check the entire form using the `.valid()` method.
    $('#entrar').on('click', function () {
        $('#ingreso').valid();
    });
    //Validamos Rut
    $('#rut').Rut({
        on_error: function () {
            $('#rut').addClass("error");
            $("#rut").after('<label id="rut-error" class="error" for="rut">Debe ingresar un RUT válido.</label>');
        }, //				on_success: function(){
        //					$('#rut').removeClass("error")
        //				},
        format_on: 'keyup'
    });
    $('#rut_olvidado').Rut({
        on_error: function () {
            $('#rut_olvidado').addClass("error");
            $("#rut_olvidado").after('<label id="rut-error" class="error" for="rut">Debe ingresar un RUT válido.</label>');
        }, //				on_success: function(){
        //					$('#rut').removeClass("error")
        //				},
        format_on: 'keyup'
    });
    $('#rut_registrarme').Rut({
        on_error: function () {
            $('#rut_registrarme').addClass("error");
            $("#rut_registrarme").after('<label id="rut-error" class="error" for="rut">Debe ingresar un RUT válido.</label>');
        }, //				on_success: function(){
        //					$('#rut').removeClass("error")
        //				},
        format_on: 'keyup'
    });
});
$("#registrarme").on("click", function () {
    $("#ingreso").fadeOut("fast", "linear");
    $("#contrasena_olvidada").fadeOut("fast", "linear");
    $("#registro").fadeIn("fast", "linear"); // .fadeToggle() // .slideToggle()
});
$("#ingresar").on("click", function () {
    $("#ingreso").fadeIn("fast", "linear");
    $("#contrasena_olvidada").fadeOut("fast", "linear");
    $("#registro").fadeOut("fast", "linear"); // .fadeToggle() // .slideToggle()
});
$("#olvide-contrasena").on("click", function () {
    $("#ingreso").fadeOut("fast", "linear");
    $("#contrasena_olvidada").fadeIn("fast", "linear"); // .fadeToggle() // .slideToggle()
});
$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
$(function () {
    var $h3s = $('.round-button').click(function () {
        $h3s.removeClass('active');
        $(this).addClass('active');
    });
});
// Iniciamos Tooltip
$('#btn-info').darkTooltip({
    gravity: 'west'
    , size: 'small'
, });