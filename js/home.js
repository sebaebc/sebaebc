//Iniciamos prettyPhoto
$(document).ready(function () {
    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false
        , show_title: true
        , allow_resize: false
        , allow_expand: true
        , theme: 'light_rounded'
    });
});
