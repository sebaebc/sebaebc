/**
 * jQuery plugin to work with Images:
 *  - (pre)load images
 *  - positionate images within a container
 *  
 *  @change 2014-12-29: Skip dimensioning if error on image load
 */
(function($){
	
	// API
	var img = {};
	
	// Load
	img.load = function(/* url, ..., ¿callback?*/){
		var args = Array.prototype.slice.call(arguments),
			urls = [],
			url, 
			callback;
		// get varargs urls
		while(typeof (url = args.shift()) === 'string' || (url.nodeType && url.src)) {
			urls.push(url.nodeType ? url.src : url);
		}
		// accept array of urls instead of varargs urls
		if( !urls.length && $.isArray(url) ) {
			urls = url;
			url = args.shift();
		}
		// user callback
		callback = $.isFunction(url) ? url : false;
		var l = urls.length,
			c = -1, 
			s = 0,
			img;
		// callback
		var cb = function(error){
			c++;
			if( error !== true ) {
				s++;
			}
			if( c === l && callback ){ 
				callback(c !== s);
			}
		};
		// error callback
		cb.error = function(){
			cb(true);
		}
		// load images
		for(var i=0; i<l; i++){
			img = new Image();
			img.onload = cb;
			img.onerror = cb.error;
			url = urls[i];
			img.src = url.nodeType && url.src || url;
		}		
		// init: handle empty urls
		cb(true);
	};
	
	// Positionate image within his container
	img.position = function(e, options) {       
		// image element
		var $img = $(e);
		// options
        var data = $img.data();
        options = $.extend({}, img.position.options, {
            crop: 	data.imgCrop,
            resize: data.imgResize,
            position: data.imgPosition,
        	fx: data.imgFx,
            duration: 	data.imgDuration
        }, typeof options === 'string' ? { position: options } : (options || {})); 
		// get position
		var position = options.position.toLowerCase().split(' ');
		position = position.length == 1 ? [position[0], 'center'] : position;
		position = img.positions[position.join(' ')] 
		if( !position) {
			throw '[$.img.position] Invalid position value: ' + options.position;
		}
		// get fx
		var fx = img.fx[options.fx];
		if( !fx) {
			throw '[$.img.position] Invalid fx value: ' + options.fx;
		}
		// resize
		var resize = options.crop || options.resize;
		// load image
		img.load(e, function(error){
			// skip if error
			if( error ) {
	    		options.load && options.load($img, error);
				return;
			}
			// init position
			$img.css({ width: 'auto', height: 'auto', margin: 0, display: 'block'});    	
			// calculate position variables
			var ih = $img.height(),
				iw = $img.width(),
				$p = $img.parent(),
				ph = $p.height(),
				pw = $p.width(),
				r = resize ? Math[options.crop ? 'max' : 'min'](ph/ih,pw/iw) : 1;
			// positionate
    		$img.css(position({
				height: ih*r,
				width: iw*r
    		}, ih*r, iw*r, ph, pw));
    		// fx
    		fx[1]($img, 0, function(){
    			fx[0]($img, options.duration, function(){
    	    		// callback
    	    		options.load && options.load($img, error);
    			});
    		});
		});
	};
	// Positions
	img.positions = {
		'left top': function(css, ih, iw, ph, pw){
			css["margin-left"] = 0;
			css['margin-top'] = 0;
			return css;
		},
		'left center': function(css, ih, iw, ph, pw){
			css["margin-left"] = 0;
			css['margin-top'] = (ph - ih) / 2;
			return css;
		},
		'left bottom': function(css, ih, iw, ph, pw){
			css["margin-left"] = 0;
			css['margin-top'] = ph - ih;
			return css;
		},
		'right top': function(css, ih, iw, ph, pw){
			css["margin-left"] = pw - iw;
			css['margin-top'] = 0;
			return css;
		},
		'right center': function(css, ih, iw, ph, pw){
			css["margin-left"] = pw - iw;
			css['margin-top'] = (ph - ih) / 2;
			return css;
		},
		'right bottom': function(css, ih, iw, ph, pw){
			css["margin-left"] = pw - iw;
			css['margin-top'] = ph - ih;
			return css;
		},
		'center top': function(css, ih, iw, ph, pw){
			css["margin-left"] = (pw - iw) / 2;
			css['margin-top'] = 0;
			return css;
		},
		'center center': function(css, ih, iw, ph, pw){
			css["margin-left"] = (pw - iw) / 2;
			css['margin-top'] = (ph - ih) / 2;
			return css;
		},
		'center bottom': function(css, ih, iw, ph, pw){
			css["margin-left"] = (pw - iw) / 2;
			css['margin-top'] = ph - ih;
			return css;
		},
		// allow use of simple 'top' and 'bottom' positions
		'top center': function(css, ih, iw, ph, pw){
			css["margin-left"] = (pw - iw) / 2;
			css['margin-top'] = 0;
			return css;
		},
		'bottom center': function(css, ih, iw, ph, pw){
			css["margin-left"] = (pw - iw) / 2;
			css['margin-top'] = ph - ih;
			return css;
		}
	}
	// Fx
	img.fx = {
    	"none": [function($e, duration, complete){
    		$e.show(0, complete);
    	}, function($e, duration, complete){
    		$e.hide(0, complete);
    	}],
    	"show": [function($e, duration, complete){
    		$e.show(duration, complete);
    	}, function($e, duration, complete){
    		$e.hide(duration, complete);
    	}],
    	"fade": [function($e, duration, complete){
    		$e.fadeIn(duration, complete);    		
    	}, function($e, duration, complete){
    		$e.fadeOut(duration, complete);    		
    	}],
    	"slide": [function($e, duration, complete){
    		$e.slideDown(duration, complete);
    	}, function($e, duration, complete){
    		$e.slideUp(duration, complete);
    	}]
    };
	// Center (& resize) Image within his container
	img.center = function(e, options){
		img.position(e, $.extend({ crop: false, resize: true, position: 'center center'}, options || {}));
	}	
	// Crop (& resize) Image within his container
	img.crop = function(e, options){
		img.position(e, $.extend({ crop: true, resize: true, position: 'center center'}, options || {}));
	}
	// Default options
	img.position.options = {
		crop: false,
		resize: false,
		position: 'left top',
		fx: 'none',
		duration: 'normal'
	};
	
	// $.fn
	$.fn.img = function(method /*, args ...*/){
		var fct = img[method],
			args = Array.prototype.slice.call(arguments, 1);
		if( !fct || !$.isFunction(fct) ) {
			throw '[$.fn.img] Invalid method: ' + method;
		}
		return this.filter('img').each(function(){
			fct.apply(img, [this].concat(args));
		}).end();
	};
	
	// Expose API
	$.img = img;
	
	// Auto-Init
	$(function(){
		// center
		$('img.img-center').img('center');
		// crop
		$('img.img-crop').img('crop');		
		// position
		$('img.img-position').img('position');		
	});
	
})(jQuery)