$(document).ready(function () {
    $('#valores').validate({
        rules: {
            colaboradores: {
                required: true
            }
            , monto: {
                required: true
            }
        , }
        , submitHandler: function (form) { // for demo
            $("#calculadora").fadeToggle();
        }
    });
    // programmatically check the entire form using the `.valid()` method.
    $('#calcular').on('click', function () {
        $('#valores').valid();
    });
});
$("#calcular").on("click", function () {
    if (!$("#valores").validate().form()) {
        return false; //doesn't validate
    }
    else {
        $("#calculadora").fadeToggle();
    }
});