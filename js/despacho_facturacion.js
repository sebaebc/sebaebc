$(document).ready(function () {
	$(function () {
		var comunas = [
      "Cerrillos"
    , "La Reina"
    , "Pudahuel"
    , "Cerro Navia"
    , "Las Condes"
    , "Quilicura"
    , "Conchalí"
    , "Lo Barnechea"
    , "Quinta Normal"
    , "El Bosque"
    , "Lo Espejo"
    , "Recoleta"
    , "Estación Central"
    , "Lo Prado"
    , "Renca"
    , "Huechuraba"
    , "Macul"
    , "San Miguel"
    , "Independencia"
    , "Maipú"
    , "San Joaquín"
    , "La Cisterna"
    , "Ñuñoa"
    , "San Ramón"
    , "La Florida"
    , "Pedro Aguirre Cerda"
    , "Santiago"
    , "La Pintana"
    , "Peñalolén"
    , "Vitacura"
    , "La Granja"
    , "Providencia"
    , ];
		$("#comuna").autocomplete({
			source: comunas
		});
	});
	$(function () {
		var regiones = [
      "Región Metropolitana"
      , "XV Arica y Parinacota"
      , "I Tarapacá"
      , "II Antofagasta"
      , "III Atacama"
      , "IV Coquimbo"
      , "V Valparaíso"
      , "VI O'Higgins"
      , "VII Maule"
      , "VIII Biobío"
      , "IX La Araucanía"
      , "XIV Los Ríos"
      , "X Los Lagos"
      , "XI Aysén"
      , "XII Magallanes y Antártica"
    ];
		$("#region").autocomplete({
			source: regiones
		});
	});
});
$(document).ready(function () {
	$('#despacho_facturacion').validate({
		rules: {
			razon_social: {
				required: true
			}
			, rut: {
				required: true
			}
			, telefono: {
				required: true
				, maxlength: 11
				, minlength: 9
			}
			, direccion: {
				required: true
			}
			, n_direccion: {
				required: true
			}
			, complemento: {
				required: true
			}
			, region: {
				required: true
			}
			, comuna: {
				required: true
			}
			, email: {
				required: true
			}
			, confirma_email:  {
				required: true
				, equalTo: '#email'
			}
		, }
		, messages: {
			rut: {
				required: "Debe ingresar un RUT."
			, }
		}
	});
	// programmatically check the entire form using the `.valid()` method.
	$('#siguiente').on('click', function () {
		$('#despacho_facturacion').valid();
	});
	$('#rut').Rut({
		on_error: function () {
			$('#rut').addClass("error");
			$("#rut").after('<label id="rut-error" class="error" for="rut">Debe ingresar un RUT válido.</label>');
		}, //				on_success: function(){
		//					$('#rut').removeClass("error")
		//				},
		format_on: 'keyup'
	});
});