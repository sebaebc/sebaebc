$(document).ready(function() {
	// Botonera para secciones
    $('#botonera-secciones a').on("click",function() {
        var target = $($(this).attr('href'));
        $('html,body').animate({
            scrollTop: target.offset().top - 80
        }, 1000);
        return false;
    });

    // Posicion Botonera Secciones
    $(window).scroll(function() {
        var targetScroll = $('#botonera-secciones').position().top;
        var currentScroll = $('html').scrollTop() || $('body').scrollTop();
        var footertotop = $('footer').position().top;
        var adtobottom = $('#botonera-secciones').position().bottom;

        if (currentScroll > 300) {
            $('#botonera-secciones').css({
                position: "fixed ",
                top: "200px "
            });
        } else {
            if (currentScroll <= 300) {
                $('#botonera-secciones').css({
                    position: "absolute ",
                    top: "520px"
                });
            }
        }

        if ($(window).scrollTop() + $(window).height() > footertotop) {
            $('#botonera-secciones').css('margin-top', 0);
        } else {
            $('#botonera-secciones').css('margin-top', 0);
        }

    });


});