$(document).ready(function () {
	// Manejo de boton para cerrar avisos
	$('#btn-cerrar-aviso').click(function () {
		if ($('#aviso').is(':visible')) {
			$('#aviso').slideUp('fast');
		}
	});
	// Chosen - Select con estilos customizados
	if ($(window).width() > 1024) {
		$('.chosen-select').chosen({
			disable_search: true
		});
	}
	// Menú de navegación en móviles
	var burger = function () {
			if ($(window).width() <= 1024) {
				// Menu de navegación
				$('#btn-menu-moviles').on("click", function () {
					if (!$('nav').is(':visible')) {
						$(this).addClass("closeNav");
						$('nav').fadeIn('fast');
						$('#form-acceso-moviles').fadeOut('fast');
					}
					else {
						$('nav').fadeOut('fast');
						$(this).removeClass("closeNav");
					}
					$('main').toggleClass('menu-open');
				});
			}
		}
		// Menú estático
	var stickyHeader = function () {
		var w = $(window).width()
			, n = $("nav")
			, state = n.data("state");
		if (!state) {
			state = (w > 1024) ? "mobile" : "desktop"; // en reversa para forzar la carga
		}
		if (w <= 1024) {
			if (state == "desktop") {
				n.data("state", "mobile");
				if ($.fn.unstick) {
					n.unstick();
				}
				burger();
				n.hide();
			}
		}
		else if (w > 1024) {
			if (state == "mobile") {
				n.data("state", "desktop");
				if ($.fn.sticky) {
					n.sticky({
						topSpacing: 0
					}).sticky('update');
				}
				// Update NAV state
				n.show();
				$('#btn-menu-moviles').off("click");
			}
		}
	}
	stickyHeader();
	$(window).resize(function () {
		/**
		 * Call stickyHeader() function to stick 'nav'
		 */
		stickyHeader();
		// Redimensiona imagenes al cambiar de tamaño la ventana
		$('img.img-crop').img('crop');
		$('img.img-center').img('center');
		// Refrescar vista
		//        if (localStorage.reloaded !== 'true' && $(window).width() < 1025) {
		//            location.reload();
		////            if (typeof (Storage) !== "undefined") {
		////                localStorage.setItem('reloaded', 'true');
		////            }
		//        }
	});
	// Manejo de ventanas modal,
	// Si existe el div #modal.load se mostrará por sobre el resto de la página
	// al cargar la página.
	if ($('#modal.load').length) {
		$(window).load(function () {
			mostrarModal();
		});
	}
	// Click en el boton .btn-modal mostrará la ventana modal que corresponda
	// según el atributo data-modal del botón
	$('.btn-modal').click(function () {
		var modal = $(this).attr('data-modal');
		mostrarModal(modal);
	});

	function mostrarModal(modal) {
		var modalTarget = '#modal';
		if (modal !== undefined) {
			modalTarget = '#modal.' + modal;
		}
		$.fancybox({
			'autoScale': true
			, 'transitionIn': 'fade'
			, 'transitionOut': 'fade'
			, 'href': modalTarget
			, 'helpers': {
				'overlay': {
					'css': {
						'background-color': 'rgba(44,178,228,0.75)'
						, 'background-image': 'none'
						, 'z-index': 99999
					}
					, 'locked': false
				}
			}
		});
		$('#modal .btn-cerrar').click(function () {
			$.fancybox.close();
		});
	}
	// iCheck
	$('input[type="radio"], input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_flat-blue'
		, radioClass: 'iradio_flat-blue'
	});
	// iCheck
	// Usado en página: /privado/vivir_bien/perfil_nutricional.html
	$(".checkbox-style1 input[type=checkbox]").iCheck({
		checkboxClass: "checkbox-custom-style1"
		, inheritClass: true
	});
	if ($(window).width() > 1024) {
		//Darktooltip
		$(".tooltipx").darkTooltip({
			trigger: "hover"
		})
		$('.tooltip').darkTooltip({
			animation: 'fadeIn'
			, gravity: 'west'
			, theme: 'light'
			, size: 'small'
		});
		$('.mapa-tooltip').darkTooltip({
			animation: 'fadeIn'
			, gravity: 'south'
			, theme: 'light'
		});
		$('.tooltip-abajo').darkTooltip({
			animation: 'fadeIn'
			, gravity: 'north'
			, theme: 'light'
			, size: 'small'
		});
	}
});
$(document).ready(function () {
	/**
	 * Carrusel de Logos en Home
	 */
	$("#grupo-productos").owlCarousel({
		itemsCustom: [
            [0, 2]
            , [350, 2]
            , [450, 3]
            , [600, 4]
            , [1024, 6]
        ]
		, pagination: false
		, autoPlay: 3000
		, stopOnhover: true
		, navigation: true
		, navigationText: [
            '<img class="prev" src="./images/general/anterior.gif" alt="Anterior">'
            , '<img class="next" src="./images/general/siguiente.gif" alt="Siguiente">'
        ]
	});
	/**
	 * Testimonials
	 * @description Para ver los videos de los testimonios en ventana modal/lightbox
	 * 
	 * @added 31/08/2016
	 */
	//$(".testimony a.testimony-video").on("click",function(e){
	$(".testimony").on("click", function (e) {
		e.preventDefault();
		var link = $("a.testimony-video", this).attr("href")
			, youtube_parser = function (url) {
				// Regex /^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/
				var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
				var match = url.match(regExp);
				return (match && match[7].length == 11) ? match[7] : false;
			}
			, link = youtube_parser(link);
		var body = $("body")
			, template = '<div id="testimonials-video">' + '<div class="video">' + '<div class="closeVideo"></div>' + '<iframe src="https://www.youtube.com/embed/' + link + '" frameborder="0" allowfullscreen></iframe>' + '</div>' + '</div>';
		// Remove any previous video
		if ($("#testimonials-video", body).length) {
			$("#testimonials-video", body).remove();
		}
		var body = body.prepend(template)
			, videoBox = $("#testimonials-video", body)
			, video = $(".video", videoBox)
			, nH = (530 * video.width()) / 760;
		video.css({
			"height": nH
		});
		videoBox.on("click", ".closeVideo", function () {
			var close = setTimeout(function () {
				videoBox.removeClass("visible").on("transitionend", function () {
					videoBox.remove();
				});
			}, 1);
		});
		var open = setTimeout(function () {
			videoBox.addClass("visible");
		}, 1);
	})
});
$(document).ready(function () {
	resizeChosen();
	$(window).on('resize', resizeChosen);
});

function resizeChosen() {
	$(".chosen-container").each(function () {
		$(this).attr('style', 'width: 100%');
	});
}